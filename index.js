var _ = require('./node_modules/underscore')._;

/* Use command line argument to output selected function*/
var selection = null;
process.argv.forEach(function (val, index, array) {
    if (index == 2)
        selection = val;    
});

const values = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,17,19,20];
const uvalues = [3,6,8,1,2,4,7,5,9,10];
const names = ['Kent','Martin','Olle','Mathias','Eva','Edit','Agnes','Malte'];

/* Map */
var map_result = _.map(names, function(name) {
    return name + ' is swedish';
});
//console.log(names); // Function does not change initial array!

/* Reduce (sum) */
var reduce_result = _.reduce([1,2,3], function(memo, num) {
    //console.log(`${memo}, ${num}`);
    return memo + num;
}); // Optional: Add initial value

var reduceRight_result = _.reduceRight([1,2,3], function(memo, num) {
    //console.log(`${memo}, ${num}`);
    return memo + num;
}); 

/* Find */
var find_result = _.find(values, function(value)
{
	return value % 2 === 0;		// Find the first even value in values
});


/* Utility */
var log = function(contents) {
    if (_.isArray(contents)) {
        _.each(contents, function(e, i, l) {
            log(e);
        });
    } else {
        console.log(contents);
    }
}


/* Results */
if (!selection || selection == 'map') {
    log(map_result);
}
if (!selection || selection == 'reduce') {    
    log(reduce_result);
}
if (!selection || selection == 'reduceright') {    
    log(reduceRight_result);
}
if (!selection || selection == 'find') {    
    log(find_result);
}


