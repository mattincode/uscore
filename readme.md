# Brown Bag - Underscore

## Get Started
> ### From scratch
1. Install node
3. `mkdir uscore` + `cd uscore`
3. `npm init` 
4. `npm i underscore`
5. create index.js and type: `var _ = require('./node_modules/underscore')._;`

> ### From this repo
1. `git clone https://titte71x@gitlab.com/titte71x/uscore.git`
2. `cd uscore`
3. `npm i`
4. `node .\index.js`

## General
* Falls backs on ES-standard implementations...
* Can be used on both arrays or on properties (on an object)

## Collections
### Reduce
* *Usage:* _Sum values_
* *Features:* _Uses memoization_
```
reduce(values, function(memo, num){
	-- can return value or object
});
```
	
	
reduceRight : Starts at opposite direction

### Find
find(values, function(value)
{
	return value % 2 === 0;		// Find the first even value in values
})

find(values, criteriaFunction).exampleValue

filter(values, evenCriteria)		// return all even elements

where(values, {key:"value"})		// Return all elements with matching key and value

findWhere(values,{key:"value"} )    // find first item with matching key and value

reject(values, evenCriteria)			// return all items that does not match the critera

every(values, function)				// executes a function for every element in array and return if all evaluates to true 
some(values, function(value, i, list))	// check if any value evaluates to true, Like any in linq

contains(values, 1)					// Check if 1 is in the values array
contains(author, 'Craig')

invoke(values, 'join')				// Invoke functions on array (join in this case is a function you have to define)
invoke(values, 'join', '|')			// Chain, several functions

pluck(values, 'name')				// Select specific values
max(values)							// calculcates sum
max(autors, maxCritera)				// Calculates based on maxCritera-iterator
sortBy(values, sortCriteria)		// More control on how to sort 


## Arrays

### First (alias: head, take)
- When passed and argument gets a specific number of items from an array

### Initial
- With no arg, get all but the last.
- When passed and argument gets all excepts the last number of items from an array

### Last
- Get the last item
- When passed and argument gets a specific number of items beginning from the end of the array

### Rest (alias: tail, drop)
- Starting from the index, get the rest of the items.

### Compact
- Removes all falsy values (null, false, 0...
*Note: Does only work on the first level, so multi-dimensional arrays is not handled*

### Flatten
- Flattens a nested array
- Can pass in true to only flatten the first level.

### Without
- Removes all items in the array that is passed in as arguments.
_.without(array, 'active', 1, ..)

### Union
- Merges two arrays, does not include duplicates

### Intersection
- Return array that only has the overlapping values

### Difference
- Take first array and removed the ones that is in the second array

### Uniq (unique)
- Returns unique values (removes duplicates)
- Can pass in true to speed up if pre-sorted.
- Can pass criteria-function

### Zip
- combines several arrays into one multi-dimensional array

### Object
- Combines a keys array with a values array to create a key-value array of objects.
- Can also pass in one multi-dimensional array with keys and values
data = [['key1', 'value1], ['key2', 'value2']]

### IndexOf
- Get index number (zero based) of supplied object/value
- Provide start index position
- Returns -1 if not found
- Can supply true if array is pre-sorted to get better performance

### LastIndexOf
- Get the last index of the supplied value/object
- Start location

### SortedIndex
- Get the position that the value/object would be positioned
- If already same value/object exists it will insert it before the same values.

### Range
- Get an array of numbers
- Start and step values are optional

_.range(1,5,2) // start at 1, end at 5, step 2.

## Functions

### Bind
- Manages reference of this pointer
- Can add extra arguments
- ES2015 has this already

### BindAll
- Binds all functions in the object
- Can add list of function-names to bind (do not bind other than these functions)

### Partial
- Can pre-pass parameters to a function

### Memoize
- Caches the result of a function with certain parameters

### Delay
- Delays a call to a function

### Defer
- Wait to execute a function until another function has been executed.

### Throttle
- Only call a function at a specific wait time...
- Ex: if a function is called 1000 times during 1000ms, it will only execute every {wait time} interval.

### Debounce
- Similar to Throttle

### Once
- Make sure the function is only called once

### After
- Used with async-calls (works like promises iin ES2015)
- Executes after an async call has completed.

### Wrap
- Associate a function with another function call, the same arguments is passed.
- Can wrap objects (used like notify, property changed)

### Compose
- Chain several functions and send result of the previous function as a result to the next.
- Starts executing from the right.

---------------------------------------------------
== 	type conversion allowed
=== no type conversion allowed
